package logika;

/**
 *  Třída PrikazMluv implementuje pro hru příkaz mluv.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazMluv implements IPrikaz {
    private static final String NAZEV = "mluv";
    private HerniPlan plan;

    /**
     *  Konstruktor třídy
     *  
     * 
     */    
    public PrikazMluv(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "mluv". Zkouší mluvit s osobou v daném prostoru. Osoba musí být v prostoru.
     *
     *@param parametry - jako  parametr obsahuje jméno osoby,
     *                         s kterou se má mluvit.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length != 1) {
            // pokud chybí druhé slovo (sousední místnost), tak ....
            return "Příkaz musí mít právě jeden parametr \"tady\"";
        }   
        Prostor kdeJsme = plan.getAktualniProstor();
        String nazevSKymMluvit = parametry[0];
        Osoba osubka = kdeJsme.vyberOsoba(nazevSKymMluvit);
        if (osubka==null){
            return "Tato osoba zde není";}
        return osubka.toString();
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     * @return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

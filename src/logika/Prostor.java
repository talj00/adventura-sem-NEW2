package logika;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.HashSet;



/**
 *  Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 *  Tato třída je součástí jednoduché textové hry.
 *
 *  "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři
 *  hry. Prostor může mít sousední prostory připojené přes východy. Pro
 *  každý východ si prostor ukládá odkaz na sousedící prostor.
 *
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */

public class Prostor {
    private String nazev;
    private String popis;
    private Set<Prostor> vychody;  // obsahuje sousední místnosti
    private Set<Vec> veci; 
    private Set<Osoba> osoby;
    private boolean jeZamceno=false;
    private boolean schovana=true;
    private String seznam;

    /**
     * Vytvoření  prostoru se zadaným popisem, např.
     * "kuchyň", "hala", "trávník před domem"
     *
     *@param  nazev     nazev prostoru, jednoznačný identifikátor, jedno slovo nebo víceslovný název bez mezer.
     *@param  popis     Popis prostoru.
     */
    public Prostor(String nazev, String popis, boolean schovana) {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<Prostor>();
        veci = new HashSet <Vec> ();
        osoby = new HashSet <Osoba> ();
        this.schovana = schovana;

    }

    /**
     * Metody vrací a umožňují pracovat s zamčeno a schovaná.
     */
    
    public boolean jeZamceno(){
        return jeZamceno;
    }
    
    private Vec klic;
     public void nastavKlic(Vec klic) {
             this.klic = klic;
       }

       public Vec getKlic() {
             return klic;
       }
    
    public void zamknout(boolean zamceno) {
        this.jeZamceno = zamceno;
    } 

    public boolean schovana() {return schovana;}

    public void videt(boolean stav) {this.schovana = stav;}

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu, že je použit
     * Set pro uložení východů, může být sousední prostor uveden pouze jednou
     * (tj. nelze mít dvoje dveře do stejné sousední místnosti). Druhé zadání
     * stejného prostoru tiše přepíše předchozí zadání (neobjeví se žádné chybové hlášení).
     * Lze zadat též cestu ze do sebe sama.
     * 
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze třídy Object.
     * Dva prostory jsou shodné, pokud mají stejný název.
     * Tato metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * 
     * Bližší popis metody equals je u třídy Object.
     *
     *@param   o  object, který se má porovnávat s aktuálním
     *@return     hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */

    @Override
    public boolean equals (Object o) {
        if (o instanceof Prostor) {
            Prostor druhy = (Prostor)o;
            return nazev.equals(druhy.nazev);
        }
        else {
            return false;
        }
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva pro optimalizaci ukladani
     * v dynamickych datovych strukturach. Pri prekryti metody equals je potreba prekryt i metodu hashCode.
     * Podrobny popis pravidel pro vytvareni metody hashCode je u metody hashCode ve tride Object
     */

    @Override
    public int hashCode() {
        return nazev.hashCode();
    }

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako
     * parametr konstruktoru)
     *
     *@return    název prostoru
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně:
     *      Jsi v mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste.
     *      vychody: chodba bufet ucebna
     *
     *@return    Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return "Jsi v mistnosti " + popis + ".\n" + seznamVychodu();
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy)například:
     * "vychody: hala ".
     *
     *@return    Seznam názvů sousedních prostorů
     */
    public String seznamVychodu() {
        String vracenyText = "vychody:";
        for (Prostor sousedni : vychody) {
            if (sousedni.schovana()) {
                break;
            }
            vracenyText += " " + sousedni.getNazev();    
            if (sousedni.jeZamceno()) {
                vracenyText += "-(zamknuto)";
            }

        }
        return vracenyText;
    }

    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním prostorem,
     * vrací se hodnota null.
     *
     *@param  nazevSouseda  Jméno sousedního prostoru (východu)
     *@return            Prostor, který se nachází za příslušným východem, nebo
     *                   hodnota null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        if (nazevSouseda == null) {
            return null;
        }
        for ( Prostor sousedni : vychody ){
            if (sousedni.getNazev().equals(nazevSouseda)) {
                return sousedni;
            }

        }
        return null;  // prostor nenalezen
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat, odebírat východy)
     * protože z hlediska správného návrhu je to plně záležitostí třídy Prostor.
     *
     *@return    Nemodifikovatelná kolekce prostorů (východů), se kterými tento prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu, že je použit
     * Set pro uložení východů, může být sousední prostor uveden pouze jednou
     * (tj. nelze mít dvoje dveře do stejné sousední místnosti). Druhé zadání
     * stejného prostoru tiše přepíše předchozí zadání (neobjeví se žádné chybové hlášení).
     * Lze zadat též cestu ze do sebe sama.
     * 
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     *
     */
    public void setVec(Vec v) {
        veci.add (v);
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy)například:
     * "vychody: hala ".
     *
     *@return    Seznam názvů sousedních prostorů
     */
    public String seznamVeci() {
        String seznam = "Věci v místnosti: ";
        if (veci.size()==0){
            seznam = seznam + " žádné věci zde nejsou.";}
        else{
            for (Vec v : veci) {
                seznam += "\n *** " + v.getNazev() + " - " + v.getPopis() + " -  váha " +v.getVaha();
            }}
        return seznam;
    }

    /**
     * Metoda vrací hodnotu true nebo false, podle toho jestli daná věc je obsažena v prostoru.
     */
    public boolean obsahujeVec (String jm){
        for (Vec v : veci) {
            if (v.getNazev().equals(jm)){
                return true;
            }
        }
        return false;
    }


    /**
     * Metoda vybere vec z prostoru.
     */
    public Vec vyberVec (String jmeno) {
        for ( Vec v : veci ){
            if (v.getNazev().equals(jmeno)) {
                Vec vybranaVec = v;
                veci.remove(vybranaVec);
                return vybranaVec;
            }}
        for (Vec v : veci ){
            if (v.jeProzkoumana() && v.seznamVeci.containsKey(jmeno)) {
            v.seznamVeci.remove(jmeno);
            }
            }
        return null;}
      
    /**
     * Metoda vybere věc z prostoru a to i když je obsažena v jiné věci.
     */
    public Vec vemVec (String jm) {
        for ( Vec v : veci ){
            if (v.getNazev().equals(jm)) {
                return v;
            }}
        for (Vec v : veci ){
            if (v.jeProzkoumana() && v.seznamVeci.containsKey(jm)) {
                Vec vec = v.seznamVeci.get(jm);
                if (vec.isPrenosna()) {
                    return vec;
                }
                return null;
            }}
        return null;
    }

    /**
     * Metoda vrací hodnotu true a false podle toho zda-li je hledaná věc v prostoru a ve věcech v daném prostoru.
     */
    public boolean jeVec(String jmeno) {
        if (najdiVecVProstoru(jmeno) == null) { // vec není v místnosti
            for (Vec vec : veci) {    // prohledávám věci v místnosti
                if (vec.obsahujeVec(jmeno)) { // když nějaká věc obsahuje věc
                    return true;
                }
            }
            return false;  // věc není ani v prozkoumaných věcech (truhlách)
        }
        else {
            return true;
        }

    }

    /**
     * Metoda hledá věc v prostoru
     */
    private Vec najdiVecVProstoru(String jmeno) {
        Vec vec = null;
        for (Vec neco : veci) {
            if (neco.getNazev().equals(jmeno)) {
                vec = neco;
                break;
            }
        }
        return vec;
    }

    /**
     * Metoda vypíše seznam osob v prostoru.
     */
    public String seznamOsob() {
        String seznam = "\n Osoby v místnosti: ";
        if (osoby.size()==0){
            seznam = seznam + " žádné osoby zde nejsou.";}
        else{
            for (Osoba u : osoby) {
                seznam += "\n *** " + u.getJmeno();
            }}
        return seznam;
    }

    /**
     * Metoda vybere osobu v prostoru.
     */
    public Osoba vyberOsoba (String jm) {
        for ( Osoba u : osoby ){
            if (u.getJmeno().equals(jm)) {

                return u;
            }}
        return null;
    }

    /**
     * Metoda přidá osobu do prostoru.
     */
    public void setOsoba(Osoba o) {
        osoby.add (o);
    }

    /**
     * Metoda vymaže osobu v daném prostoru.
     */
    public Osoba vymazOsobu(String jm) {
        for ( Osoba u : osoby ){
            if (u.getJmeno().equals(jm)) {
                Osoba vybranaOsoba = u;
                osoby.remove(u);
                return vybranaOsoba;
            }}
        return null;
    }

}


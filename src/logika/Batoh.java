package logika;

import java.util.Set;
import java.util.HashSet;
/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */

/*******************************************************************************
 * Instance třídy Batoh představují ...
 *
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class Batoh 
{
    private Set <Vec> veci;
    private int maxVeci;
    private int maxVaha;
    private int seznam;
    

    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public Batoh()
    {
        veci = new HashSet <Vec> ();
        this.maxVeci = maxVeci;
        this.maxVaha = maxVaha;
    }

    /**
     * Metoda nastavuje maximum věcí, které mohou být v batohu najednou.
     */
    public void setVeci(int n){this.maxVeci = n;}

    /**
     * Metoda přidává věc do batohu.
     */
    public void setVec(Vec v) {
        veci.add (v);
    }

    /**
     * Metoda nastavuje maximální váhu věcí, která může být v batohu najednou.
     */
    public void setVaha(int nos){this.maxVaha = nos;}

    /**
     * Metoda vrací aktuální váhu batohu.
     */
    public int vahaVeci (){
        for (Vec v : veci) {
            return v.getVaha();
        }   
        return seznam;
    }

    /**
     * Metoda vrací hodnotu zda-li se může věc přidat do batohu.
     * Záleží na počtu věcí v batohu a váze věcí v batohu.
     */
    public boolean vejdeSeVec (Vec v) {   
        if (veci.size() >= maxVeci){
            return false;}
        if ((vahaVeci()+v.getVaha()) >= maxVaha) {
            return false;}
        return true;
        }
    

    /**
     * Vrací textový řetězec, který popisuje sousední východy)například:
     * "vychody: hala ".
     *
     *@return    Seznam názvů sousedních prostorů
     */
    public String seznamVeci() {
        String seznam = "Věci v batohu: ";
        if (veci.size()==0){
            seznam = seznam + " žádné věci zde nejsou.";}
        else{
            for (Vec v : veci) {
                seznam += "\n *** " + v.getNazev() + " - " + v.getPopis() + " -  váha " +v.getVaha();
            }}
        return seznam;
    }
    
    /**
     * Metoda kontroluje zda-li je věc v batohu.
     */
    public boolean obsahujeVec (String jm){
        for (Vec v : veci) {
            if (v.getNazev().equals(jm)){
                return true;
            }
        }
        return false;
    }

    /**
     * Metoda vybere a odstraní věc z batohu.
     */
    public Vec vyberVec (String jm) {
        for ( Vec v : veci ){
            if (v.getNazev().equals(jm)) {
                Vec vybranaVec = v;
                veci.remove(v);
                return vybranaVec;
            }}
        return null;
    }

    /**
     * Metoda vybere věc z batohu.
     */
    public Vec vemVec (String jm) {
        for ( Vec v : veci ){
            if (v.getNazev().equals(jm)) {
                return v;
            }}
        return null;
    }

    /**
     * Metoda vrací věc.
     */
    public Vec jeVec (Vec v) {

        if (v.getNazev().equals(v)){
            return v;
        }
        return null;
    }
    
    public Set getBatoh() {
        return veci;
    }
}

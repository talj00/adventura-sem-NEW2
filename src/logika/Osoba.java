package logika;

/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */

/*******************************************************************************
 * Instance třídy Osoba představují ...
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class Osoba
{
    private String jmeno;
    private String proslov;
    private boolean probehla=false;
    private String recNechci;
    private String recPo;
    private String recKonec;
    private Vec coChce;
    private Vec coDa;

    /***************************************************************************
     *  Konstruktor ....
     */
    public Osoba(String jmeno, String proslov, String recNechci, String recPo, String recKonec, Vec coChce, Vec coDa)
    {
        this.jmeno = jmeno;
        this.proslov = proslov;
        this.recNechci = recNechci;
        this.recPo = recPo;
        this.recKonec = recKonec;
        this.probehla = probehla;
        this.coChce = coChce;
        this.coDa = coDa;
    }

    /**
     * Metody které vrací řeči osoby a věci, které chce a dá.
     */
    public String getJmeno(){return jmeno;}

    public String getProslov(){return proslov;}

    public String getNechci(){return jmeno+": "+recNechci;}

    public String getChci(){return jmeno+": "+recPo;}

    public Vec getCoChce(){return coChce;}

    public Vec getCoDostane(){return coDa;}

    public boolean getProbehla() {return probehla;}

    public void probehla(boolean probehla) {this.probehla=probehla;}

    /**
     * Metoda vrací řeč osoby po příkazu mluv (proslov nebo konečnou řeč[závisí na "probehla"]). 
     */
    public String toString() {
        if (probehla){
            return jmeno+": "+recKonec;
        }
        return jmeno+": "+proslov;
    }

    /**
     * Metoda vrací řeč osoby po výměně.
     */
    public String getPo(){
        if (probehla){
            return jmeno+": "+recPo;
        }
        return null;
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze třídy Object.
     * Dva prostory jsou shodné, pokud mají stejný název.
     * Tato metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * 
     * Bližší popis metody equals je u třídy Object.
     *
     *@param   o  object, který se má porovnávat s aktuálním
     *@return     hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */

    @Override
    public boolean equals (Object p) {
        if (p instanceof Osoba) {
            Osoba druha = (Osoba)p;
            return jmeno.equals(druha.jmeno);
        }
        else {
            return false;
        }
    }
    public int hashCode(){
        return 12;}

}
    

package logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;

    /**
     *  Konstruktor třídy
     *  
     *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
     */    
    public PrikazJdi(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadané místnosti. Pokud místnost
     *  existuje, vstoupí se do nové místnosti. Pokud zadaná sousední
     *  místnosti (východ) není, vypíše se chybové hlášení.
     *
     *@param parametry - jako  parametr obsahuje jméno místnosti,
     *                         do které se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední místnost), tak ....
            return "Kam mám jít? Musíš zadat jméno místnosti";
        } 
        String smer = parametry[0];
        Prostor kdeJsme = plan.getAktualniProstor();
        Prostor sousedniProstor = kdeJsme.vratSousedniProstor(smer);
        if(sousedniProstor == null || sousedniProstor.schovana()){
            return "Tam se odsud jit neda!";}
        if (sousedniProstor.jeZamceno()) {
            return "dveře do místnosti "+sousedniProstor.getNazev()
            +" jsou zamčené";
        }
        plan.setAktualniProstor(sousedniProstor);
        return sousedniProstor.dlouhyPopis();

    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

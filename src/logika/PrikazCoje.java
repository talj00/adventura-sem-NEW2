package logika;

/**
 *  Třída PrikazCoje implementuje pro hru příkaz coje.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazCoje implements IPrikaz {
    private static final String NAZEV = "coje";
    private HerniPlan plan;
    private Batoh batoh;

    /**
     *  Konstruktor třídy
     *  
     * 
     */    
    public PrikazCoje(Batoh batoh, HerniPlan plan) {
        this.plan = plan;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "coje". Rozhledne se po místnosti a vypíše osoby a věci v prostoru nebo
     *  pokud je druhý parametr batoh vypíše věci v batohu.
     * 
     *
     *@param parametry - jako  parametr obsahuje tady nebo batoh,
     *                         podle toho co se má prozkoumat.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length != 1) {
            // pokud chybí druhé slovo (sousední místnost), tak ....
            return "Příkaz musí mít jeden parametr \"tady\" nebo \"batoh\"";
        }   

        String kde = parametry[0];
        if (kde.equals("tady")) {
            return plan.getAktualniProstor().seznamVychodu()+"\n"+plan.getAktualniProstor().seznamVeci()+plan.getAktualniProstor().seznamOsob();

        }
        if (kde.equals("batoh")) {
            return batoh.seznamVeci();
        }
        return "Příkaz musí mít jeden z techto tvaru:\n"+"\t\"coje tady\" nebo \"coje batoh\"";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

package logika;

/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */

import java.util.*;
/*******************************************************************************
 * Instance třídy Vec představují ...
 *
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class Vec
{
    private String nazev;
    private String popis;
    private boolean prenosna = false;
    private int vaha;
    public Map<String,Vec> seznamVeci;
    private boolean prozkoumana = false;
    /***************************************************************************
     *  Konstruktor ....
     */
    public Vec(String nazev, String popis, boolean prenosnost, int vaha, boolean prozkoumana)
    {
        this.nazev = nazev;
        this.popis = popis;
        this.prenosna = prenosnost;
        this.vaha = vaha;
        this.prozkoumana = prozkoumana;
        seznamVeci = new HashMap<String,Vec>();

    }

    /**
     * Metody, které umožňují pracovat s přenosná, váha, název, popis, prozkoumaná a jídlo.
     * Za pomoci těchto metod se dají upravovat hodnoty přenosná, váha, název, popis, prozkoumaná a jídlo.
     */
    public void setPrenosnost(boolean n) { prenosna = n;}

    public boolean isPrenosna(){return prenosna;}

    public int getVaha() {return vaha;}

    public String getNazev(){return nazev;}

    public String getPopis(){return popis;}

    public boolean jeProzkoumana() {return prozkoumana;}

    public void prozkoumano (boolean prozkoumana) {this.prozkoumana = prozkoumana;}

    public void vlozVec (Vec neco) {seznamVeci.put(neco.getNazev(), neco);}

    /**
     * Metoda smaže věc a vloží jí do proměné vec.
     */
    public Vec vyberVec(String jmeno) {
        Vec vec = null;
        if (prozkoumana && seznamVeci.containsKey(jmeno)) {
            vec = seznamVeci.get(jmeno);
            if (vec.isPrenosna()) {
                seznamVeci.remove(jmeno);
            }
        }
        return vec;
    }

    /**
     * Metoda vrací text po příkazu prozkoumej "věc" a vypíše co obsahuje.
     */    
    public String popisProzkoumej() {
        if (seznamVeci.isEmpty()) {
            return "prohlédl jsi si pozorně "+nazev+" a je to normální "+nazev+" nic víc.";
        }
        String popis = "Prohlédl jsi si pozorně "+nazev+" a uvnitř jsi našel:";
        for (String jmeno : seznamVeci.keySet()) {
            popis += " " + jmeno+".";
        }
        return popis;
    }

    /**
     * Tato metoda prohledává ve věcích další věci
     */  
    public boolean obsahujeVec(String jmeno) {
        return prozkoumana && seznamVeci.containsKey(jmeno);
    }

    /**
     * Metoda vrací název věci a její popis.
     */
    public String toString() {
        return nazev+" ("+popis+")";
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze třídy Object.
     * Dva prostory jsou shodné, pokud mají stejný název.
     * Tato metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * 
     * Bližší popis metody equals je u třídy Object.
     *
     *@param   o  object, který se má porovnávat s aktuálním
     *@return     hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */

    @Override
    public boolean equals (Object o) {
        if (o instanceof Vec) {
            Vec druha = (Vec)o;
            return nazev.equals(druha.nazev);
        }
        else {
            return false;
        }
    }
    public int hashCode(){
        return 12;}

}

package logika;

import logika.Batoh;
import logika.HerniPlan;
import logika.IPrikaz;
import logika.Prostor;

/**
 *  Třída PrikazOdemkni implementuje pro hru příkaz odemkni.
 *  
 */
public class PrikazOdemkni implements IPrikaz {
    private static final String NAZEV = "odemkni";
    private HerniPlan plan;
    private Batoh batoh;

    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, je potřeba zjistit, zda jsem v místnosti
    *                    vedle místnosti, která se má odemknout
    */    
    public PrikazOdemkni(HerniPlan plan, Batoh batoh) {
        this.plan = plan;
        this.batoh = batoh;
    }

    /**
     *  Provádí příkaz "odemkni". Zjišťuji, zda z aktuální místnosti je
     *  východ do zadané místnosti. Pokud místnost existuje a je zamčená,
     *  tak se zjistí, zda v batohu je potřebný klíč. Pokud ano, odemknu
     *  místnost.
     *
     *@param parametry - jako  parametr obsahuje jméno místnosti,
     *                         do které se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední místnost), tak ....
            return "Co mám odemknout? Musíš zadat jméno místnosti";
        }

        String prostor = parametry[0];

        // hledám zadanou místnost mezi východy
        Prostor sousedi = plan.getAktualniProstor();
        Prostor sousedniProstor = sousedi.vratSousedniProstor(prostor);

        if (sousedniProstor == null) {
            return "Odsud nevedou dveře do místnosti "+prostor+" !";
        }
        else {
            if (sousedniProstor.jeZamceno()) {
                if (!batoh.obsahujeVec("klic")) {
                    return "Pro odemčení dveří do "+ prostor +" potřebuješ mít "
                            + "u sebe klíč";
                }
                else {
                    sousedniProstor.zamknout(false);
                    return "Podařilo se ti otevřít dveře do místnosti "
                            + prostor + ". Nyní je cesta volná.";
                }
            }
            else {
                return "Místnost "+ prostor +" již byla odemčená!!!";
            }
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @return nazev prikazu
     */
     @Override
    public String getNazev() {
        return NAZEV;
    }

}
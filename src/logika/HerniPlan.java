package logika;


/**
 *  Class HerniPlan - třída představující mapu adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class HerniPlan {

    private Prostor aktualniProstor;

    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }

    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    private void zalozProstoryHry() {
        Prostor prostor;
        // vytvářejí se jednotlivé prostory
        Prostor hrad = new Prostor("hrad","tajemný hrad.\n"
            +  "Aby ses dostal zpět domů, musíš najít klíč.\n"
            +  "Prozkoumej hrad a najdi klíč!",false); 
        Prostor zahradka = new Prostor("zahradka","Zahrádka plná kvetoucích květin a jabloní. U skleníku stojí jeden pán.\n"
            + "Bude to asi místní zahradník.\n",false);
        Prostor chodba = new Prostor("chodba","Chodba vedoucí do knihovny nebo do komnaty1.\n"
            + "Zdá se, že tu nejsí sám. \n"
            + "Před vstupem do knihovny sedí skřítek.\n"
            + "Promluv si s ním.\n",false);
        Prostor knihovna = new Prostor("knihovna","Vstoupil jsi do veliké knihovny. Můžeš si knihy prohlédnout a jednu si i vzít.",false);
        Prostor komnata1 = new Prostor("komnata1","Nikdo tady asi nebydlí...",false);
        Prostor komnata2 = new Prostor("komnata2","Něco tady žlutě svítí... že by už byl den?"
                + "Vypada to na nějaký žlutý amulet...\n",false);        
        Prostor komnata3 = new Prostor("komnata3","Vypadáto, že tu někdo nedávno byl..",false);      
        Prostor tajemna_komnata = new Prostor("tajemna_komnata","Právě jsi vstoupil do veliké temné místnosti. Nejsi tu ale sám."
            + "Něco se k tobě přibližuje...\n"
            + "Vypadá to na... draka!\n"
            + "Rychle, udělej něco nebo bude po tobě!! \n",false);       
        Prostor domu = new Prostor("domu", "Doma. Konečně!! Děkuji, že jste si zahráli. Pro ukončení napište slovo konec.",false);
        
        // přiřazují se průchody mezi prostory (sousedící místnosti)
        hrad.setVychod(zahradka);
        hrad.setVychod(chodba);
        zahradka.setVychod(hrad);        
        chodba.setVychod(hrad);
        chodba.setVychod(knihovna);
        chodba.setVychod(komnata1);
        knihovna.setVychod(chodba);        
        komnata1.setVychod(komnata2); 
        komnata1.setVychod(chodba);
        komnata2.setVychod(komnata1);
        komnata2.setVychod(komnata3);
        komnata3.setVychod(komnata2);
        komnata3.setVychod(tajemna_komnata);
        tajemna_komnata.setVychod(domu);
        tajemna_komnata.zamknout(true);
        
        
        // vytvářejí se věci
        Vec jablko = new Vec ("jablko","",true,10,true);
        Vec cervena_kniha  = new Vec ("cervena_kniha","",true,30,true);
        Vec bryle  = new Vec ("bryle","",true,5,true);
        Vec truhla  = new Vec ("truhla","",false,65,false);
        Vec zluty_amulet  = new Vec ("zluty_amulet","",true,20,true);
        Vec portal  = new Vec ("portal","",false,15,false);
        Vec mec  = new Vec ("mec","",true,30,true);
        Vec skrin  = new Vec ("skrin","",false,80,false);
        Vec saty  = new Vec ("saty","",true,5,true);
        Vec klic  = new Vec ("klic","",true,10,true);
        
        //přiřazují se věci do prostoru
        zahradka.setVec (jablko);
        knihovna.setVec (cervena_kniha);
        //knihovna.setVec (bryle);
        tajemna_komnata.setVec (truhla);
        komnata2.setVec (zluty_amulet);
        komnata3.setVec (mec);
        komnata3.setVec (skrin);
        komnata3.setVec (saty);
        zahradka.setVec(klic);
        
        
        
        
        
        // vytvářejí se věci v daných prostorech
        chodba.setVec (new Vec("nazev_2","popisek", true, 2,true));
        // vytvářejí se osoby v daných prostorech
                    
        knihovna.setOsoba(new Osoba("skritek", "Citím jablka.. krásná šťavnatá jablka... Přineseš mi nějaká? Něco ti za to dám.", "To nechci.", "Děkuji!!! Tady máš brýle. Prý je shání zahradník.", "Hmm citim jablka intenzivně.", jablko, bryle));
        tajemna_komnata.setOsoba(new Osoba("drak", "ARGGHHHH ", "ARGGHHHHH ", "ARGGHHHH","ARGGHHHH", null, null ));
        zahradka.setOsoba(new Osoba("zahradnik", "Někde jsem ztratil brýle.. Nevíš kde jsou? Potřebuji je. ", "Ne, to nejsou moje brýle. ", "Děkuji. Za to ti dám klíče. Můžou se ti hodit.","ARGGHHHH", bryle, klic));
        
        
        // vytvářejí se věci ve věcech (např. papír v kredenci)
        //vec.vlozVec (new Vec("nazev_3","popisek", true, 2,true));

        aktualniProstor = hrad;  // hra začíná v prostoru nora

    }
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */

    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }

    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
    }

}

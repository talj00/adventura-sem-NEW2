package logika;

/**
 *  Třída PrikazSeber implementuje pro hru příkaz seber.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazSeber implements IPrikaz {
    private static final String NAZEV = "seber";
    private HerniPlan plan;
    private Batoh batoh;
    private Hra hra;

    /**
     *  Konstruktor třídy
     *  
     * 
     */    
    public PrikazSeber(Batoh batoh, HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.batoh = batoh;
        this.hra = hra;

    }

    /**
     *  Provádí příkaz "seber". Sebere danou věc v místnosti. Pokud je v místnosti a nebo je 
     *                          ve věci, která je již prozkoumána.
     *
     *@param parametry - jako  parametr obsahuje jméno věci,
     *                         která se má sebrat.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim, co mam sebrat.";
        }
        if (parametry.length == 2) {
            return "Mohu vzít pouze jednu věc na jednou.";
        }
        Prostor kdeJsme = plan.getAktualniProstor();
        String nazevCoVzit = parametry[0];
        Vec vecicka = kdeJsme.vemVec(nazevCoVzit);
        if (vecicka==null){
            return "Tato věc tu neni";}
        if (nazevCoVzit.equals("pasticka")){
            hra.setKonecHry(true);
            return "*cvak* A je po myšičce :(";
        }
        if (!vecicka.isPrenosna()){
            return "Tato vec nejde prenest.";}
        if (!batoh.vejdeSeVec(vecicka)){
            return "Tohle se ti už do batohu nevejde";}
        if (!vecicka.jeProzkoumana()){
            return "Tato věc tu není";}
        batoh.setVec (vecicka);
        kdeJsme.vyberVec(nazevCoVzit);
        return " vec \""+nazevCoVzit+"\" byla dána do batohu.";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

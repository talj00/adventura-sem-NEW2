package logika;


/**
 *  Třída PrikazPoloz implementuje pro hru příkaz poloz.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 * @author    Vojtěch Lovecký
 * @version   1.00.000
 */
public class PrikazPoloz implements IPrikaz {
    private static final String NAZEV = "poloz";
    private HerniPlan plan;
    private Batoh batoh;
    private Hra hra;

    /**
     *  Konstruktor třídy
     *  
     */    
    public PrikazPoloz(Batoh batoh, HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.batoh = batoh;
        this.hra = hra;

    }

    /**
     *  Provádí příkaz "poloz". Pokládá věc do daného prostoru. Věc musí být v batohu a 
     *  musí být povoleno jí zde položit(případ nora)
     *
     *@param parametry - jako  parametr obsahuje jméno věci,
     *                         která se má položit.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String proved(String... parametry) {
        if (parametry.length == 0) {
            return "Nevim, co mam položit.";
        }
        if (parametry.length == 2) {
            return "Mohu položit pouze jednu věc na jednou.";
        }
        Prostor kdeJsme = plan.getAktualniProstor();
        String nazevCoPolozit = parametry[0];
        Vec vecicka = batoh.vemVec(nazevCoPolozit);
        if (vecicka==null){
            return "Tato vec v batohu neni";}

        batoh.vyberVec(nazevCoPolozit);
        kdeJsme.setVec (vecicka);
        return " vec \""+nazevCoPolozit+"\" byla vyndana z batohu.";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}

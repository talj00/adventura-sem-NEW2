package main;

/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */

import iuText.TextoveRozhrani;

/*******************************************************************************
 * Instance třídy Start představují ...
 *
 * @author   jtm
 * @version   1.00.000
 */
public class Start
{
    /****************************
     * Konstruktor
     */
    

    /**
     * Metoda pro nové textové rozhraní.
     */
      public static void main (String [] args) {
        TextoveRozhrani textove = new TextoveRozhrani();
        textove.hraj();

    }


}
